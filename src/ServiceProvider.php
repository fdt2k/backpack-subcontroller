<?php

namespace KDA\Backpack\Subcontroller;


use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
   
    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasTranslations;

    protected $packageName='kda.subcontroller';

    protected $viewNamespace = 'kda-backpack-subcontroller';
    protected $publishViewsTo = 'vendor/kda/backpack/subcontroller';
    
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    public function register(){
        
      

        parent::register();
    }

    protected function bootSelf(){
     


    }


   

}
