<?php

namespace KDA\Backpack\Subcontroller\Traits;

use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use PhpParser\Node\Stmt\ElseIf_;

trait SubCrudController
{
    
    use CustomizableFormHeader;
    function setParentController($parent)
    {
        
        $this->crud->macro('getSubControllerValues', function () {
            
            $value = request()->query('subcontrollers');
            $value = json_decode(base64_decode($value), true);
            
            return $value ?? [];
        });
        
        $this->crud->macro('getSubControllerCurrentValues', function () {
            
            $value = request()->query('subcontrollers');
            $value = json_decode(base64_decode($value), true) ?? [];
            $value = array_pop($value);
            
            return $value;
        });
        
        $this->crud->macro('getSubControllerPreviousValues', function () {
            
            $value = request()->query('subcontrollers');
            $value = json_decode(base64_decode($value), true) ?? [];
            
            array_pop($value);
            return base64_encode(json_encode($value));
        });
        $this->crud->macro('getSubControllerValuesSinceIndex', function ($i) {
            
            $value = request()->query('subcontrollers');
            $value = json_decode(base64_decode($value), true) ?? [];
            \Log::debug('getSubControllerValuesSinceIndex ' . $i, array_slice($value, 0, $i));
            $value = array_slice($value, 0, $i);
            return base64_encode(json_encode($value));
        });
        
        $this->crud->macro('getSubControllerCurrentKeyValue', function ($key) {
            
            $values = $this->getSubControllerCurrentValues();
            $values = $values['args'] ?? [];
            $key_value = $values[$key] ?? NULL;
            return $key_value;
        });
        
        $this->crud->macro('getParentController', function () use ($parent) {
            
            return $parent;
        });
        
        
        
        $this->crud->macro('getSubControllerQuerystring', function ($value = NULL) use ($parent) {
            /*  $key = $parent['key'];
            
            
            $key_value = request()->query($key);
            return  $key . '=' . $key_value;*/
            $qs = $value ?? request()->query('subcontrollers');
            return 'subcontrollers=' . $qs;
        });
        
        
        $this->crud->macro('getSubControllerUrl', function ($url = '') use ($parent) {
            $key = $parent['key'];
            
            
            return  url($this->route . $url . '?' . $this->getSubControllerQuerystring());
        });
        $this->crud->macro('getPreviousSubControllerUrl', function ($url = '') use ($parent) {
            $key = $parent['key'];
            
            
            return  url($this->route . $url . '?subcontrollers=' . $this->getSubControllerPreviousValues());
        });
        
        $this->crud->macro('getBackSubControllerUrl', function ($url = '') use ($parent) {
           
            
            if ($this->getOperationSetting('subcontroller.parentIsHandlingList')) {
                return backpack_url($this->getParentController()['route'] . '?' . $this->getSubControllerQuerystring($this->getSubControllerPreviousValues()));
            }
           /* $key = $parent['key'];

            $key_value = request()->query($key);*/
            return  url($this->route . $url . '?' . $this->getSubControllerQuerystring());
        });
        
        $this->crud->macro('applySubControllerListClause', function () use ($parent) {
            $key = $parent['key'] ?? NULL;
            $clause = $parent['clause'];
            if(is_callable($parent['clause'])){
                $parent['clause']($this,$this->getSubControllerCurrentValues());
            }else if ($key) {
                $key_value = $this->getSubControllerCurrentKeyValue($key);
                if ($clause && $key_value) {
                    $this->addClause($clause, $key_value);
                }
            }
            
        });
        $this->crud->macro('isSubcontrolled', function () use ($parent) {
            //       $key = $parent['key'];
            /*$clause = $parent['clause'];
            
            if ($key) {
                $key_value = request()->query($key);
                if($key_value){
                    return true;
                }
            }*/
            return $this->getSubControllerCurrentValues() != NULL;
        });
        $this->crud->macro('getParentControllerHiddenFields', function () {
            $parent = $this->getParentController();
            if(is_callable($parent['key'])){
                return  $parent['key']($this,$this->getSubControllerCurrentValues());

                
            }else{
                $key = $parent['key'];
                //$key_value = request()->query($key);
                
                
                $key_value = $this->getSubControllerCurrentKeyValue($key);
                return [[
                    'name'  => $key,
                    'type'  => 'hidden',
                    'value' => $key_value,
                    ]];
                }
            });
        }
        
        public function setupSubController($opts = [])
        {
            //    dump($this->crud->getSubControllerCurrentValues());
            $this->setupCustomizableFormHeaderPlugin();
            $parent = $opts['parent'] ?? NULL;
            if ($parent) {
                $this->setParentController($parent);
            }
            
            $this->crud->setOperationSetting('subcontroller.parentIsHandlingList', $opts['parentIsHandlingList'] ?? false);
            $this->crud->setCreateView('kda-backpack-subcontroller::backpack.crud.create');
            $this->crud->setEditView('kda-backpack-subcontroller::backpack.crud.edit');
            
            if (version_compare(\Composer\InstalledVersions::getPrettyVersion('backpack/crud'), '5.1', '>=')) {
                $this->crud->setReorderView('kda-backpack-subcontroller::backpack.51.crud.reorder');
            } else {
                $this->crud->setReorderView('kda-backpack-subcontroller::backpack.crud.reorder');
            }
            \Log::info('all chain', [$this->crud->getSubControllerValues()]);
            \Log::info('current values', [$this->crud->getSubControllerCurrentValues()]);
        }
        
        public function setupSubControllerReorderOperation()
        {
            $this->crud->applySubControllerListClause();
        }
        public function setupSubControllerListOperation()
        {
            $this->crud->setOperationSetting('resetButton', false);
            
            $this->crud->applySubControllerListClause();
            $this->setupBreadCrumbsSubController();
            if ($this->crud->hasButtonWhere('name', 'create')) {
                $this->crud->removeButton('top', 'create');
                $this->crud->addButton('top', 'create', 'view', 'kda-backpack-subcontroller::backpack.crud.buttons.create');
            }
            if ($this->crud->hasButtonWhere('name', 'update')) {
                $this->crud->removeButton('line', 'update');
                $this->crud->addButton('line', 'update', 'view', 'kda-backpack-subcontroller::backpack.crud.buttons.update');
            }
            if ($this->crud->hasButtonWhere('name', 'reorder')) {
                $this->crud->removeButton('top', 'reorder');
                $this->crud->addButton('top', 'reorder', 'view', 'kda-backpack-subcontroller::backpack.crud.buttons.reorder');
            }
        }
        
        public  function setupBreadCrumbsSubController()
        {
            foreach ($this->crud->getSubControllerValues() as $i => $controller) {
                \Log::debug('bc', [base64_decode($this->crud->getSubControllerValuesSinceIndex($i))]);
                
                $this->data['breadcrumbs'][$controller['parent_name']] = backpack_url($controller['parent'] . '?subcontrollers=' . $this->crud->getSubControllerValuesSinceIndex($i));
            }
            $this->data['breadcrumbs'][$this->crud->entity_name_plural] = false;
        }
        
        public function setupSubControllerCreateOperation()
        {
            CRUD::removeAllSaveActions();
            
            $this->crud->addSaveAction([
                'name' => 'save_action_one',
                'redirect' => function ($crud, $request, $itemId) {
                    return $crud->getSubControllerUrl();
                },
                'button_text' => trans('kda.subcontroller::subcontroller.create'),
                'visible' => function ($crud) {
                    return true;
                },
                'referrer_url' => function ($crud, $request, $itemId) {
                    return $crud->route;
                },
                'order' => 1,
            ]);
            
            $this->crud->setOperationSetting('showCancelButton', false);
            $fields = $this->crud->getParentControllerHiddenFields();
            
            foreach ($fields as $field) {
                if (CRUD::hasFieldWhere('name', $field['name'])) {
                    CRUD::removeField($field['name']);
                }
                CRUD::addField($field);
            }
        }
        public function setupSubControllerUpdateOperation()
        {
            CRUD::removeAllSaveActions();
            $this->setupBreadCrumbsSubController();
            $this->crud->addSaveAction([
                'name' => 'save_action_two',
                'redirect' => function ($crud, $request, $itemId) {
                    return $crud->getBackSubControllerUrl();
                },
                'button_text' => trans('kda.subcontroller::subcontroller.save'),
                'visible' => function ($crud) {
                    return true;
                },
                'referrer_url' => function ($crud, $request, $itemId) {
                    return $crud->route;
                },
                'order' => 1,
            ]);
            
            $this->crud->setOperationSetting('showCancelButton', false);
            $fields = $this->crud->getParentControllerHiddenFields();
            
            foreach ($fields as $field) {
                CRUD::addField($field);
            }
        }
    }
    