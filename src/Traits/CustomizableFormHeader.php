<?php

namespace KDA\Backpack\Subcontroller\Traits;

use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

trait CustomizableFormHeader
{
    public function setupCustomizableFormHeaderPlugin(){
        $this->crud->macro('getFormHeaderBackString',function(){
            return $this->get('form-header.string') ?? NULL;
        });
        $this->crud->macro('setFormHeaderBackString',function($s){
            $this->set('form-header.string',$s);
        });

        $this->crud->macro('setFormHeaderView', function ($view) {
            $this->set('form-header.view', $view);
        });
        $this->crud->macro('getFormHeaderView', function () {
            return $this->get('form-header.view')?? 'kda-backpack-subcontroller::backpack.crud.inc.form-header';
        });
    }
}