<?php

namespace KDA\Backpack\Subcontroller\Operations;

use Illuminate\Support\Facades\Route;

trait BackOperation
{
 
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupBackRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/back', [
            'as'        => $routeName . '.back',
            'uses'      => $controller . '@back',
            'operation' => 'back',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupBackDefaults()
    {
        $this->crud->allowAccess('back');
        $this->crud->setOperationSetting('label', 'Retour', 'sc_back');
        $this->crud->setOperationSetting('route', '', 'sc_back');
        $this->crud->setOperationSetting('nested', false);
        $this->crud->setOperationSetting('display', true);
        
        $this->crud->operation('back', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('top', 'back', 'view', 'kda-backpack-subcontroller::backpack.crud.buttons.back');
        });
    }



    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function back()
    {
      
        return redirect(backpack_url('order'));
    }
}
