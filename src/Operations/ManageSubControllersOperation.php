<?php

namespace KDA\Backpack\Subcontroller\Operations;

use Illuminate\Support\Facades\Route;
use Arr;
trait ManageSubControllersOperation
{
  

    function setSubControllers($subcontrollers,$route='')
    {
        $this->crud->macro('getSubControllers', function () use ($subcontrollers) {

            return $subcontrollers;
        });
        $this->crud->macro('getSCRoute', function () use ($route) {

            return $route;
        });
    }

    function setGetSubEntry($callback)
    {
        $this->crud->macro('getSubEntry',$callback);
    }


    protected function setupManageSubControllersRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/{id}/{controller}/managesubcontroller', [
            'as'        => $routeName . '.managesubcontroller',
            'uses'      => $controller . '@managesubcontroller',
            'operation' => 'managesubcontroller',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupManageSubControllersDefaults()
    {
        $this->crud->macro('pushSubControllerArg',function($route,$args){
        
            $value = request()->query('subcontrollers') ;
            $value = json_decode(base64_decode($value),true);
            if(!$value){
                $value = [];
            }
            $value[]=[
                'args'=>$args,
                'route'=> $route,
                'parent'=>$this->getSCRoute(),
                'parent_name'=> $this->entity_name_plural,
            ];
            $value = base64_encode(json_encode($value));
            return $value;
        });
        $this->crud->allowAccess('managesubcontroller');

        $this->crud->operation('managesubcontroller', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'managesubcontroller', 'view', 'kda-backpack-subcontroller::backpack.crud.buttons.managesubcontroller');
        });
    }



    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function managesubcontroller($id,$controller_key)
    {
        $controllers = $this->crud->getSubControllers();
        $controller = $controllers[$controller_key];
        if(is_callable($controller['key'])){
            $key = $controller['key']($id);
        }else {
            $key = $controller['key'] ?? 'id';
        }
        $url = $controller_key;
        $subaction = $controller['subaction'] ?? NULL;
        $subaction_key = $controller['subaction_key'] ?? NULL;
       //dd($this->crud->getRoute());
        if( $subaction ){
            if($subaction_key){
                $entry= $this->crud->getSubEntry($id);

                $val = Arr::get($entry,$subaction_key);
                $url.="/".$val;
            }
            $url.="/".$controller['subaction'];
        }

        //$url .='?'.$key.'='.$id;
        if(!is_array($key)){
            $key = [$key=>$id];
        }
        $args = $this->crud->pushSubControllerArg($controller_key,$key);
        return redirect(backpack_url($url.'?subcontrollers='.$args));
    }

    
}
