<?php

namespace KDA\Backpack\Subcontroller\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;
class PluginController extends CrudController
{

    public function __construct(){
        if ($this->crud) {
            return;
        }
        parent::__construct();
        $this->middleware(function ($request, $next) {
            $uses = class_uses_recursive($this);
            return $next($request);
        });
    }
    
}