@php
$controllers = $crud->getSubControllers();

$subControllerShouldDisplay = function () {
    return true;
};

if ($crud->hasMacro('subControllerShouldDisplay')) {
    $subControllerShouldDisplay = function ($entry) use ($crud) {
        return $crud->subControllerShouldDisplay($entry);
    };
}
@endphp

@foreach ($controllers as $key => $controller)
    @if ($crud->hasAccess($controller['operation_name'] ?? 'managesubcontroller') && $subControllerShouldDisplay($entry))
        <a href="{{ url($crud->route .'/' .$entry->getKey() .'/' .$key .'/managesubcontroller?subcontrollers=' .request()->query('subcontrollers')) }}"
            class="btn btn-sm btn-link" data-style="zoom-in"><span class="ladda-label"><i
                    class="la la-box-open"></i>{{ $controller['label'] }}</span></a>
    @endif
@endforeach
