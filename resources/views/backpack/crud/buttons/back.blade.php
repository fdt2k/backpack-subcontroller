@php
	$parent = $crud->getParentController();
	$key = $parent['key'];
	$key_value = request()->query($key);
	$nested = $crud->get('back.nested');
	$display = $crud->get('back.show');

	$url = backpack_url($parent['route']) ;

	if($nested ){
		$url = $crud->getPreviousSubControllerUrl();
	}
@endphp	
	@if($display)
	<a href="{{ $url }}" class="btn  btn-primary" data-style="zoom-in"><span class="ladda-label">{{$crud->getOperationSetting('sc_back.label')??'Retour'}}</span></a>
	@endif