@if ($crud->hasAccess('list'))
    @php
        $back_string = $crud->getFormHeaderBackString() ?? trans('backpack::crud.back_to_all') . '<span>' . $crud->entity_name_plural . '</span>'; //
    @endphp
    <small>
        <a href="{{ $crud->getBackSubControllerUrl() }}" class="d-print-none font-sm"><i
                class="la la-angle-double-{{ config('backpack.base.html_direction') == 'rtl' ? 'right' : 'left' }}"></i>
            {!! $back_string !!}
        </a>
    </small>
@endif
